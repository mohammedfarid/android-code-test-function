package com.farid.testgooglemap;

import java.util.ArrayList;

import android.app.Activity;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.view.Menu;
import android.widget.Toast;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

public class MapActivity extends Activity {
	GoogleMap map;
	SQLiteDatabase db;
	LatLng sydney;
	LatLng cairo;
	LatLng paris;
	LatLng town;
	Cursor cursor;
	ArrayList<String> townnames;
	ArrayList<String> lats;
	ArrayList<String> lngs;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_map);
		// map
		map = ((MapFragment) getFragmentManager().findFragmentById(R.id.map))
				.getMap();
		//Init ArrayLists
		townnames=new ArrayList<String>();
		lats=new ArrayList<String>();
		lngs=new ArrayList<String>();
		// create database
		db = openOrCreateDatabase("Peoples", MODE_PRIVATE, null);
		// create table
		db.execSQL("CREATE TABLE IF NOT EXISTS towns(id INTEGER primary key autoincrement, townname varchar(20) not null unique, lat varchar(16) not null, lng varchar(16) not null);");
		//Insert 
		db.execSQL("INSERT OR IGNORE INTO  towns(townname, lat,lng) VALUES ('aswan', '24.088938','32.8998293');");
		//select & show
		cursor = db.rawQuery("SELECT * FROM towns", null);				
		cursor.moveToFirst();
		do{
			townnames.add(cursor.getString(cursor.getColumnIndex("townname")));
			lats.add(cursor.getString(cursor.getColumnIndex("lat")));
			lngs.add(cursor.getString(cursor.getColumnIndex("lng")));
			cursor.moveToNext();
		}while(cursor.isAfterLast() == false);
		//Close
		db.close();
		//show on map
		//map.moveCamera(CameraUpdateFactory.zoomTo(21));
		 Toast.makeText(getApplicationContext(),
					townnames.get(0)+"    " + lats.get(0)+"   "+lngs.get(0), Toast.LENGTH_SHORT).show();
		town=new LatLng(Double.parseDouble( lats.get(0)), Double.parseDouble(lngs.get(0)));
		sydney = new LatLng(-33.8674869, 151.2069902);
		cairo = new LatLng(30.00994962730087, 31.202545166015625);
		paris = new LatLng(48.856614, 2.3522219);
		map.addMarker(new MarkerOptions().title("aswan")
				.snippet("The most populous city in Egypt.").position(town));
		map.addMarker(new MarkerOptions().title("Sydney")
				.snippet("The most populous city in Australia.")
				.position(sydney));

		map.addMarker(new MarkerOptions().title("cairo")
				.snippet("The most populous city in Egypt.").position(cairo));
		map.addMarker(new MarkerOptions().title("paris")
				.snippet("The most populous city in France .").position(paris));


	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.map, menu);
		return true;
	}

}
