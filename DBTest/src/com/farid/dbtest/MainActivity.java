package com.farid.dbtest;

import android.app.Activity;
import android.database.Cursor;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.widget.Toast;

public class MainActivity extends Activity {
	DBAdapter db;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		db = new DBAdapter(this);

		
	}

	public void DisplayContact(Cursor c) {
		Toast.makeText(
				this,
				"id: " + c.getString(0) + "\n" + "Name: " + c.getString(1)
						+ "\n" + "Email: " + c.getString(2), Toast.LENGTH_LONG)
				.show();
	}

	public void onClickAdd(View v) {
		// ---add a contact---
		db.open();
		long id = db.insertContact("Wei-Meng Lee",
				"weimenglee@learn2develop.net");
		id = db.insertContact("Mary Jackson", "mary@jackson.com");
		db.close();
	}

	public void onClickGetAll(View v) {
		// ---get all contacts---
		db.open();
		Cursor c = db.getAllContacts();
		if (c.moveToFirst()) {
			do {
				DisplayContact(c);
			} while (c.moveToNext());
		}
		db.close();
	}

	public void onClickGet2(View v) {
		// ---get a contact---
		db.open();
		Cursor c = db.getContact(2);
		if (c.moveToFirst())
			DisplayContact(c);
		else
			Toast.makeText(this, "No contact found", Toast.LENGTH_LONG).show();
		db.close();
	}

	public void onClickUpdate(View v) {
		// ---update contact---
		db.open();
		if (db.updateContact(1, "Wei-Meng Lee", "weimenglee@gmail.com"))
			Toast.makeText(this, "Update successful.", Toast.LENGTH_LONG)
					.show();
		else
			Toast.makeText(this, "Update failed.", Toast.LENGTH_LONG).show();
		db.close();
	}

	public void onClickDelete(View v) {
		// ---delete a contact---
		db.open();
		if (db.deleteContact(3))
			Toast.makeText(this, "Delete successful.", Toast.LENGTH_LONG)
					.show();
		else
			Toast.makeText(this, "Delete failed.", Toast.LENGTH_LONG).show();
		db.close();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

}
