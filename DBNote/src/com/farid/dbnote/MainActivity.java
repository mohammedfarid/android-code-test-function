package com.farid.dbnote;

import java.util.ArrayList;

import android.os.Bundle;
import android.app.Activity;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.view.Menu;

public class MainActivity extends Activity {
	SQLiteDatabase db;
	Cursor cursor;
	ArrayList<String> title;
	ArrayList<String> titleBackground;
	ArrayList<String> text;
	ArrayList<String> textBackground;
	String s_title="xx", s_text="xx";
	double d_titleBackground=50, d_textBackground=20;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		// Init ArrayLists
		title = new ArrayList<String>();
		titleBackground = new ArrayList<String>();
		text = new ArrayList<String>();
		textBackground = new ArrayList<String>();

		// create database
		db = openOrCreateDatabase("Notes", MODE_PRIVATE, null);
		// create table
		db.execSQL("CREATE TABLE IF NOT EXISTS note(id INTEGER primary key autoincrement, title varchar(200) not null,titleBackground varchar(20) not null,text varchar(20) not null,textBackground varchar(20) not null );");
		// Insert
		db.execSQL("INSERT OR IGNORE INTO  note(title,titleBackground,text,textBackground) VALUES ('"
				+ s_title.toLowerCase()
				+ "','"
				+ String.valueOf(d_titleBackground)
				+ "', '"
				+ s_text.toLowerCase()
				+ "','"
				+ String.valueOf(d_textBackground) + "');");
		// select & show
		cursor = db.rawQuery("SELECT * FROM note", null);
		cursor.moveToFirst();
		do {
			try {
				title.add(cursor.getString(cursor.getColumnIndex("title")));
				textBackground.add(cursor.getString(cursor
						.getColumnIndex("titleBackground")));
				text.add(cursor.getString(cursor.getColumnIndex("text")));
				textBackground.add(cursor.getString(cursor
						.getColumnIndex("textBackground")));
				cursor.moveToNext();
			} catch (Exception e) {
				e.printStackTrace();
			}
		} while (cursor.isAfterLast() == false);
		// Close
		db.close();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

}
